from flask import Flask, render_template, request
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.sql import func
from form import LoginForm
import os

app = Flask(__name__)

# change to name of your database; add path if necessary
DB_NAME = 'database/sample.db'

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + DB_NAME
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
app.config['SECRET_KEY'] = os.urandom(32)

# this variable, db, will be used for all SQLAlchemy commands
db = SQLAlchemy(app)


class Goods(db.Model):
    # TODO: Move to separate file
    __tablename__ = 'goods'
    id: int = db.Column(db.Integer, primary_key=True)
    name: str = db.Column(db.String)
    price: int = db.Column(db.Integer)
    quantity: int = db.Column(db.Integer)
    last_updated: str = db.Column(db.String)


class Users(db.Model):
    __tablename__ = 'users'
    id: int = db.Column(db.Integer, primary_key=True)
    email: str = db.Column(db.String)
    password: str = db.Column(db.String)


@app.route("/", methods=["GET", "POST"])
def home():
    form = LoginForm()
    if request.method == 'POST':
        user = Users(id=__get_max_user_id(db) + 1, email=form.email.data, password=form.password.data)
        __insert_user(db, user)
        print(form.email.data)
        print(form.password.data)

    return render_template("root.html", goods_list=__get_goods(), form=form)

def __get_goods():
    try:
        goods = Goods.query.order_by(Goods.id).all()
        return goods
    except Exception as e:
        # e holds description of the error
        error_text = "<p>The error:<br>" + str(e) + "</p>"
        hed = '<h1>Something is broken.</h1>'
        return hed + error_text

def __get_users():
    try:
        users = Users.query.order_by(Users.id).all()
        return users
    except Exception as e:
        # e holds description of the error
        error_text = "<p>The error:<br>" + str(e) + "</p>"
        hed = '<h1>Something is broken.</h1>'
        return hed + error_text

def __get_max_user_id(db: SQLAlchemy) -> int:
    return db.session.query(func.max(Users.id)).scalar()

def __insert_user(db: SQLAlchemy, user: Users):
    db.session.add(user)
    db.session.commit()

if __name__ == "__main__":
    app.run(debug=True)
